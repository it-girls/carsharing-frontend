# Stage 1
# Create image based on the official Node 8 image from dockerhub
FROM node:12 as builder

# Create a directory where our app will be placed
RUN mkdir -p /opt/ng-app

# Change directory so that our commands run inside this new directory
WORKDIR /opt/ng-app

# Copy dependency definitions
COPY package*.json ./

# Install dependecies
RUN npm install

# Get all the code needed to run the app
COPY . .

# Run the angular in product
RUN npm run build


### STAGE 2: Setup ###
FROM nginx:1.14.1-alpine

## Copy our default nginx config
COPY ./nginx.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /opt/ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
