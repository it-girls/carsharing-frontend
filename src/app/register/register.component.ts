import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Userdata } from '../userdata';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm : FormGroup;

  confirmPassword: string;

  constructor(private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router) { }


  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      birthdate: [new Date(), Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      id_number: ['', Validators.required],
      license_number: ['', Validators.required]
    })
  }

  onSubmit(userdata: Userdata): void{
    if(this.registerForm.valid){
      console.log(userdata);
      userdata.birthdate.setHours(12);
      if(userdata.password == this.confirmPassword){
        this.apiService.addUser(userdata).subscribe((response) => console.log(response));
        this.registerForm.reset();
        this.router.navigateByUrl('/login');
      }
    }
  }

}
