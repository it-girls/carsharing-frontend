import { Component, OnInit, ViewChild } from '@angular/core';
import { Booking } from '../booking';
import { ApiService } from '../api/api.service';
import { Userdata } from '../userdata';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs';
import { MatSelectChange } from '@angular/material/select';
import { Vehicle } from '../vehicle';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  @ViewChild('table') table: MatTable<Booking>;
  public price_per_hour = 10.00;

  public addBookingForm: FormGroup;

  public bookings: Booking[] = [];
  public users: Userdata[] = [];

  public vehicles: Vehicle[] = [];
  public selectedVehicle: Vehicle = null;
  public selectedVehicleId: string = "";

  public columnsToDisplay = ['start_date','end_date','status','fee','comment', 'delete'];
  public selectedUser: string = '';

  public userPower$: Observable<number>;

  constructor(private apiService: ApiService,
    private authService: AuthService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userPower$ = this.authService.getPower;

    this.apiService.getBookingsForUser().subscribe(
      (response: Booking[]) => this.updateBookings(response)
    );
    this.apiService.getVehicles().subscribe(
      (response: Vehicle[]) => this.vehicles = response
      );

      this.addBookingForm = this.formBuilder.group({
        start_date: [new Date(), Validators.required],
        end_date: [new Date(), Validators.required],
        comment: ['', Validators.required],
      });

      if(this.authService.getPowerNumber() >= 3){
        this.apiService.getAllUsers().subscribe((response: Userdata[]) => this.users = response);
      }
  }

  getBookingsForUser(event: MatSelectChange): void{
    this.selectedUser = event.value;
    this.apiService.getBookingsForUser(event.value).subscribe(
      (response: Booking[]) => this.updateBookings(response)
      )
  }

  updateBookings(books: Booking[]): void{
    this.bookings = books;
    this.updateTable();
  }

  deleteBooking(booking: Booking): void{
    if(confirm("Bist du sicher, dass der Eintrag gelöscht werden soll?")){
      this.apiService.deleteBooking(booking.public_booking_id).subscribe(
        () => this.apiService.getBookingsForUser(this.selectedUser).subscribe(
          (response: Booking[]) => this.updateBookings(response)
        )
      );
    }
  }

  addBooking(booking: Booking): void{
    if(this.selectedVehicleId){
      if(this.selectedUser){
        booking.public_user_id = this.selectedUser;
      } else {
        booking.public_user_id = "";
      }
      let timeDiff = (booking.end_date.getTime() - booking.start_date.getTime());
      if(timeDiff > 0){
        let hours = (timeDiff/1000)/3600;
        booking.fee = ((Math.trunc(hours)+1) * this.price_per_hour);
        booking.status = 'aktiv';
        booking.public_vehicle_id = this.selectedVehicleId;

        this.apiService.addBooking(booking).subscribe(
          () => this.apiService.getBookingsForUser(this.selectedUser).subscribe(
            (response: Booking[]) => this.updateBookings(response)
          )
        );
      }
    }
  }

  cancelBooking(booking: Booking): void{
    this.apiService.cancelBooking(booking).subscribe(
      () => this.apiService.getBookingsForUser(this.selectedUser).subscribe(
        (response: Booking[]) => this.updateBookings(response)
      )
    );
  }

  updateTable(){
    this.table.renderRows();
  }

  areTimesValid(start: Date, end: Date): boolean{
    if((end.getTime() - start.getTime() > 0))
    return true;
  }

  updateSelectedVehicle($event: MatSelectChange) : void{
    this.selectedVehicleId = $event.value.public_vehicle_id;
  }
}
