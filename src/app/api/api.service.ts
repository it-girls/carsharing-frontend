import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs';
import { Userdata } from '../userdata';
import { Booking } from '../booking';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getUserData(): Observable<Object>{

    let url = environment.apiUrl + '/users/' + this.authService.getTokenAsJson()['public_user_id'];
    let request = this.http.get(url);

    return request;
  }

  changeUserData(user: Userdata, changePassword: boolean): void{
    let url = environment.apiUrl + '/users/';
    if(!user.public_user_id){
      url += this.authService.getTokenAsJson()['public_user_id'];
    } else {
      url += user.public_user_id;
    }

    if(!changePassword){
      delete user['password'];
    }
    let request = this.http.put(url, JSON.stringify(user));
    request.subscribe();
  }

  addUser(user: Userdata): Observable<Object> {
    let url = environment.apiUrl + '/users';

    let request = this.http.post(url, user);

    return request;
  }

  getAllUsers(): Observable<Object> {
    let url = environment.apiUrl + '/users';

    let request = this.http.get(url);

    return request;
  }

  getBookingsForUser(public_user_id?: string): Observable<Object>{
    let url = environment.apiUrl + '/users/';
    if(public_user_id == "" || !public_user_id){
      url += this.authService.getTokenAsJson()['public_user_id'];
    } else {
      url += public_user_id;
    }
    url += '/bookings';

    let request = this.http.get(url);

    console.log(url);
    return request;
  }

  addBooking(booking: Booking): Observable<Object> {
    let url = environment.apiUrl + '/bookings';

    if(!booking.public_user_id){
      booking.public_user_id = this.authService.getTokenAsJson()['public_user_id'];
    }
    let request = this.http.post(url, JSON.stringify(booking));
    return request;
  }

  deleteBooking(public_booking_id: string): Observable<Object> {
    let url = environment.apiUrl + '/bookings/' + public_booking_id;

    let request = this.http.delete(url);

    return request;
  }

  cancelBooking(booking: Booking): Observable<Object> {
    if(!booking.public_user_id){
      booking.public_user_id = this.authService.getTokenAsJson()['public_user_id'];
    }
    let url = environment.apiUrl + '/users/' + booking.public_user_id + '/bookings/' + booking.public_booking_id + '/cancel';

    let request = this.http.put(url,'');

    return request;
  }

  getVehicles(): Observable<Object>{
    let url = environment.apiUrl + '/vehicles';

    let request = this.http.get(url);

    return request;
  }

}
