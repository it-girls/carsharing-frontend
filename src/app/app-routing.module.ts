import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { MainPageComponent } from './main-page/main-page.component';
import { RegisterComponent } from './register/register.component';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { BookingsComponent } from './bookings/bookings.component';


const routes: Routes = [
  { path: 'home', component: MainPageComponent},
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardService], data: {minPower: 0, maxPower: 0} },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuardService], data: {minPower: 0, maxPower: 0}},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService], data: {minPower: 1, maxPower: 100}},
  { path: 'manageUsers', component: EditUsersComponent, canActivate: [AuthGuardService], data: {minPower: 1, maxPower: 100}},
  { path: 'bookings', component: BookingsComponent, canActivate: [AuthGuardService], data: {minPower: 1, maxPower: 100}},
  { path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
