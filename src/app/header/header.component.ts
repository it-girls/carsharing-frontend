import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() public title: string;

  isUserLoggedIn$: Observable<boolean>;
  userPower$: Observable<number>;

  constructor(private authService: AuthService) {  }


  ngOnInit() {
    this.userPower$ = this.authService.getPower;
  }

  onLogout(){
    this.authService.logout();
  }

}
