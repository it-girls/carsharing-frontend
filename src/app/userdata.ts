export interface Userdata {
  public_user_id?: string,
  email?: string,
  password?: string,
  first_name?: string,
  last_name?: string,
  birthdate?: Date,
  id_number?: string,
  license_number?: string,
  user_role?: string
}
