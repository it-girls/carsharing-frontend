import { Component, OnInit } from '@angular/core';
import { Userdata } from '../userdata';
import { ApiService } from '../api/api.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {

  public editUserForm: FormGroup;

  public users: Userdata[] = [];

  public newPassword: string = "";
  public newPasswordRepeated: string = "";

  public newUserData: Userdata = {};

  public selectedUser: Userdata = null;

  constructor(private apiService: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.apiService.getAllUsers().subscribe((response: Userdata[]) => this.users = response);
    this.editUserForm = this.formBuilder.group({
      first_name: '',
      last_name: '',
      birthdate: new Date(),
      email: '',
      id_number: '',
      license_number: ''
    })
  }

  updateValues(event: MatSelectChange): void{
    this.editUserForm.patchValue(event.value);
  }

  writeNewData(userData: Userdata): void{
    let needUpdate = false;
    for(let key in userData){
      if(userData[key] != this.selectedUser[key] && userData[key] != "" ){
        this.selectedUser[key] = userData[key];
        if(key == 'birthdate'){
          this.selectedUser['birthdate'].setHours(12);
        }
        needUpdate = true;
      }
    }
    if(needUpdate){
      this.apiService.changeUserData(this.selectedUser, false);
    }
  }

}
