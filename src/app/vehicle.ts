export interface Vehicle {
  public_vehicle_id?: string,
  chassis_number?: string,
  license_plate?: string,
  station_name?: string,
  comment?: string,
  brand?: string,
  model?: string,
  category_flag?: string,
  category_description?:string,
  vehicle_type_flag?: string,
  vehicle_type_description?: string,
  transmission_flag?: string,
  transmission_description?: string,
  addition_flag?: string,
  addition_description?: string
}
