import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm;

  public email: string = "";
  public password: string = "";
  public returnUrl: string = "";

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { }

  @Input() error: string | null;

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['',Validators.required],
      password:  ['',Validators.required]
    })
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }


  login(data: any): void{
    if(this.loginForm.valid)
      this.authService.login(data.email, data.password, this.returnUrl);
  }

}
