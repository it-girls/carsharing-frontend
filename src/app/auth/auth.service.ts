import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private x_access_token: string;
  private power = new BehaviorSubject<number>(0);


  get getPower(){
    return this.power.asObservable();
  }

  getPowerNumber():number{
    return this.power.getValue();
  }

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  login(email: string, password: string, returnURl?: string){
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Basic ' + btoa(email + ':' + password));

    let request = this.http.get(environment.apiUrl + '/login',{headers: headers});
    request.subscribe(
      (responses) => {
        this.setToken(responses['token']);
        this.router.navigateByUrl(returnURl);
      },
      (error) => {
        console.log(error);
      });
  }

  logout() {
    this.x_access_token = '';
    this.power.next(0);
  }

  getToken(): string{
    return this.x_access_token;
  }

  getTokenAsJson(): any{
    return JSON.parse(atob(this.x_access_token.split('.')[1]))
  }

  private setToken(token: string): void{
    this.x_access_token = token;
    let p = +this.getTokenAsJson()['power'];
    this.power.next(p);
  }
}
