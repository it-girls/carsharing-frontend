export interface Booking {
  public_booking_id?: string,
  public_user_id?: string,
  public_vehicle_id?: string,
  start_date?: Date,
  end_date?: Date,
  status?: string,
  fee?: number,
  comment?: string
}
