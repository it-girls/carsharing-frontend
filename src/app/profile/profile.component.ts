import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Userdata } from '../userdata';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public user: Userdata = {};

  public newPassword: string = "";
  public newPasswordRepeated: string = "";

  public newEmail: string = "";

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.updateData();
  }

  updateData(): void{
    this.apiService.getUserData().subscribe((response) => this.fillInData(response));
  }

  writeNewData(): void{
    if(this.newEmail != ""){
      this.user.email = this.newEmail;
      this.apiService.changeUserData(this.user, false);
    }
  }

  changePassword(): void{
    if(this.newPassword == this.newPasswordRepeated && this.newPassword != ""){
      this.user['password'] = this.newPassword;
      console.log(JSON.stringify(this.user));
      this.apiService.changeUserData(this.user, true);
    }
  }

  fillInData(data: Object): void{
    this.user.first_name = data['first_name'];
    this.user.last_name = data['last_name'];
    this.user.email = data['email'];
    this.user.birthdate = data['birthdate'];
    this.user.license_number = data['license_number'];
    this.user.id_number = data['id_number'];
  }
}
